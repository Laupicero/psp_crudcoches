package com.example.crudcoches.controllers;

import android.content.Context;

import com.example.crudcoches.model.DAOCoches;
import com.example.crudcoches.pojo.Coche;
import java.util.List;

public class CtrlPpl {

    //Nos cargará nuestra DB
    public static void inicializarDB_DAO(Context contx) {
        DAOCoches.createInstance(contx);
    }

    // Nos devuelve el listado de los coches
    public static List<Coche> getListadoCoches() {
        return DAOCoches.getInstance().getCoches();
    }

    // Nos devuelve el listado de las matriculas los coches
    public static List<String> getListadoMatriculas() {
        return DAOCoches.getInstance().getMatriculasCoches();
    }

    // Nos devuelve las amtrículas los coches en formato de 'array'
    public static String[] getArrayMatriculas() {
        return getListadoMatriculas().toArray(new String[0]);
    }

    // Nos devuelve un coche a partir de su matrícula
    public static Coche getCochebyMatricula(String matricula) {
        return DAOCoches.getInstance().getCocheByMatricula(matricula);
    }


    // Insertamos un nuevo coche en nuestra DB
    public static int insertarNuevocoche(String marca, String modelo, String matricula) {
        Coche coche = new Coche(marca, modelo, matricula);
        return DAOCoches.getInstance().insertar_Coche(coche);
    }


    //Nos modifica los datos de nuestro coche seleccionado en nuestra DB
    public static int modificarDatosCoche(String marca, String modelo, String matricula) {
        Coche coche = new Coche(marca, modelo, matricula);
        return DAOCoches.getInstance().modificar_Coche(coche);
    }

    // Nos elimina un coche y sus datos de nuestra DB, a través de la matrícula
    public static int eliminarDatosCoche(String matricula) {
        return DAOCoches.getInstance().eliminar_Coche(matricula);
    }
}
