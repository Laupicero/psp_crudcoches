package com.example.crudcoches.model;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.example.crudcoches.pojo.Coche;
import java.util.ArrayList;
import java.util.List;

// Para usar la DB Embebida debemos además tocar el 'graddle' de app/src
public class DAOCoches extends SQLiteOpenHelper {
    private static DAOCoches singleton;
    private static final String DB= "concesionario";
    private static final int DBVersion = 1;

    //Constructor
    private DAOCoches(Context context) {
        super(context, DB, null, DBVersion);
    }

    // Crear instancia única de la DB
    public static void createInstance(Context context){
        singleton = new DAOCoches(context);
    }

    //Obtener Singleton
    public static DAOCoches getInstance(){
        return singleton;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //Sentencia SQL para crea la tabla
        String sqlCrearTablaVehiculo = "CREATE TABLE IF NOT EXISTS Vehiculo (COD_VEHICULO INT, MARCA TEXT, MODELO TEXT, MATRICULA TEXT)";
        db.execSQL(sqlCrearTablaVehiculo);

        //Recibirá la respuesta de la DB
        // Que si no encuentra las tuplas nos creará la DB
        Cursor cursor = db.rawQuery("SELECT * FROM Vehiculo", null);

        //Sino existen tuplas
        if(!cursor.moveToFirst()) {
            //Insertamos
            db.execSQL("INSERT INTO Vehiculo (COD_VEHICULO, MARCA, MODELO, MATRICULA) VALUES (1, 'Renault', 'Zoe', '4582FDF')");
            db.execSQL("INSERT INTO Vehiculo (COD_VEHICULO, MARCA, MODELO, MATRICULA) VALUES (2, 'Renault', 'Fluence', '8255RDF')");
            db.execSQL("INSERT INTO Vehiculo (COD_VEHICULO, MARCA, MODELO, MATRICULA) VALUES (3, 'Ford', 'Escort', '457EDR')");
            db.execSQL("INSERT INTO Vehiculo (COD_VEHICULO, MARCA, MODELO, MATRICULA) VALUES (4, 'Ford', 'Fiesta', '4287YTF')");
            db.execSQL("INSERT INTO Vehiculo (COD_VEHICULO, MARCA, MODELO, MATRICULA) VALUES (5, 'Seat', 'Panda', '3435FDW')");
            db.execSQL("INSERT INTO Vehiculo (COD_VEHICULO, MARCA, MODELO, MATRICULA) VALUES (5, 'Seat', 'Ibiza', '7482DDQ')");
        }
        cursor.close();
    }



    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //Aqui eliminaremos la tabla anterior y la crearemos de nuevo vacía dependiento de la version de la DB
    }


    //-------------------------
    // MÉTODOS
    //-------------------------

    //Nos devuelve el listado completo de los coches
    public List<Coche> getCoches(){
        List<Coche> coches = new ArrayList<Coche>();

        String query = "SELECT * FROM Vehiculo";
        // Para escribir getWritableDatabase()
        SQLiteDatabase db = getReadableDatabase(); //Para leer
        Cursor cursor = db.rawQuery(query, null);

        if(cursor.moveToFirst()){
            do {
                int codVehiculo = cursor.getInt(0);
                String marca = cursor.getString(1);
                String modelo = cursor.getString(2);
                String matricula = cursor.getString(3);

                Coche c = new Coche(codVehiculo, marca, modelo, matricula);
                coches.add(c);
            }while (cursor.moveToNext());
        }
        cursor.close();
        return coches;
    }


    //Nos devuelve el listado completo de las matriculas
    public List<String> getMatriculasCoches(){
        List<String> matriculas = new ArrayList<String>();

        String query = "SELECT MATRICULA FROM Vehiculo";
        SQLiteDatabase db = getReadableDatabase(); //Para leer

        Cursor cursor = db.rawQuery(query, null);

        if(cursor.moveToFirst()){
            do {
                String matricula = cursor.getString(0);
                matriculas.add(matricula);
            }while (cursor.moveToNext());
        }
        cursor.close();
        return matriculas;
    }


    // Nos devuelve un coche a través de su matrícula
    public Coche getCocheByMatricula(String matricula) {
        Coche coche = new Coche();

        SQLiteDatabase db = getReadableDatabase();
        String sql = "SELECT * FROM Vehiculo WHERE MATRICULA = '" + matricula + "'";
        Cursor cursor = db.rawQuery(sql, null);

        if (cursor.moveToFirst()) {
            coche.setCodCoche(cursor.getInt(0));
            coche.setMarca(cursor.getString(1));
            coche.setModelo(cursor.getString(2));
            coche.setMatricula(cursor.getString(3));
        }
        cursor.close();
        return coche;
    }


    //------------
    // Nos inserta un Coche en nuestra DB
    public int insertar_Coche(Coche coche) {
        int res = 0;

        try{
            int currentID = getCurrentID();
            SQLiteDatabase db = getWritableDatabase();
            // Comprobamos primero que no exista
            boolean existeEntrada = cheking_CocheExiste_in_DB(coche.getMatricula());

            if (existeEntrada)
                res = -1;

            else {

                db.execSQL("INSERT INTO Vehiculo (COD_VEHICULO, MARCA, MODELO, MATRICULA) " +
                        "VALUES (" + ++currentID + ", '" + coche.getMarca() + "', '" + coche.getModelo() + "', '" + coche.getMatricula() + "')");

                res = 1;
            }

        }catch(Exception e){
            res = 0;
        }finally {
            return res;
        }
    }


    //------------
    // Nos modifica uno de nuestros coches de nuestra DB
    public int modificar_Coche(Coche coche){
        int res = 1;

        try{
            SQLiteDatabase db = getReadableDatabase();

            String sqlUp = "UPDATE Vehiculo SET MARCA = '" + coche.getMarca() +
                    "', MODELO = '" + coche.getModelo() + "' " +
                    ", MATRICULA = '" + coche.getMatricula() + "' " +
                    " WHERE MATRICULA = '" + coche.getMatricula() +"'";

            db.execSQL(sqlUp);

        }catch (Exception e){
            res = 0;
        }
        return res;
    }


    //------------
    // Nos elimina uno de nuestros coches de nuestra DB a traves de la matricula
    public int eliminar_Coche(String matricula){
        int res = 1;

        try{
            SQLiteDatabase db = getReadableDatabase();;

            String query = "DELETE FROM Vehiculo WHERE MATRICULA = '" +matricula+ "'";
            db.execSQL(query);

        }catch (Exception e){
            res = 0;
        }
        return res;
    }




    //-------------------------
    // MÉTODOS APOYO
    //-------------------------

    //------------
    // Nos comprueba si el coche ya existe a través de la matrícula
    private boolean cheking_CocheExiste_in_DB(String matricula) {
        boolean coincidence = false;
        String sql = "SELECT MATRICULA FROM Vehiculo";

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(sql, null);

        if (cursor.moveToFirst()) {
            do {
                if (cursor.getString(0).equalsIgnoreCase(matricula))
                    coincidence = true;
            } while (cursor.moveToNext());
        }
        cursor.close();
        return coincidence;
    }


    //------------
    // Nos obtiene el último 'ID' de los coches de nuestra DB
    // Para que al insertarlos sea autonumérico
    private int getCurrentID() {
        int coincidence = 0;
        String sql = "SELECT COD_VEHICULO FROM Vehiculo ORDER BY COD_VEHICULO DESC LIMIT 1";

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(sql, null);

        if (cursor.moveToFirst())
            coincidence = cursor.getInt(0);

        cursor.close();
        return coincidence;
    }
}
