package com.example.crudcoches.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.example.crudcoches.pojo.Coche;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.crudcoches.R;
import java.util.List;

public class CocheAdapter extends RecyclerView.Adapter<CocheAdapter.ViewRow> {
    private final List<Coche> cocheList;
    private OnClickCocheListener listener;

    public CocheAdapter(List<Coche> coches) { this.cocheList = coches; }

    public interface OnClickCocheListener{ void onItemClicked(Coche item);}
    public void setListener(OnClickCocheListener listener) {
        this.listener = listener;
    }


    @NonNull
    @Override
    public ViewRow onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerviewrowcoche, parent, false);
        return new ViewRow(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewRow holder, int position) {
        holder.bind(cocheList.get(position));
    }

    @Override
    public int getItemCount() {
        return cocheList.size();
    }

    //Clase Interna 'viewRow' que nos ayudará a implementar el RCView
    // haciendo 'binding' de los componentes del layout.plantilla
    public class ViewRow extends RecyclerView.ViewHolder {
        private final TextView tvCocheMarca, tvCocheModelo, tvCocheMatricula;
        private final LinearLayout rvFilaPlantilla;

        //Cosntructor
        public ViewRow(View itemView) {
            super(itemView);

            tvCocheMarca = itemView.findViewById(R.id.tvCocheMarca);
            tvCocheModelo = itemView.findViewById(R.id.tvCocheModelo);
            tvCocheMatricula = itemView.findViewById(R.id.tvCocheMatricula);
            rvFilaPlantilla = itemView.findViewById(R.id.rvFilaPlantilla);
        }

        public void bind(Coche coche) {
            tvCocheMarca.setText(coche.getMarca());
            tvCocheModelo.setText(coche.getModelo());
            tvCocheMatricula.setText(coche.getMatricula());

            rvFilaPlantilla.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null){
                        listener.onItemClicked(coche);
                    }
                }
            });
        }
    }

}
