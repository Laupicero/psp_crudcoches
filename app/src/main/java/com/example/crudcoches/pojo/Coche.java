package com.example.crudcoches.pojo;

import java.io.Serializable;

public class Coche implements Serializable {
    private int codCoche;
    private String marca;
    private String modelo;
    private String matricula;

    //Constructores

    //Vacío
    public Coche(){}

    // Todos los parámetros
    public Coche(int codCoche, String marca, String modelo, String matricula) {
        this.codCoche = codCoche;
        this.marca = marca;
        this.modelo = modelo;
        this.matricula = matricula;
    }

    // Todos los parámetros excepto el 'código'
    public Coche(String marca, String modelo, String matricula) {
        this.marca = marca;
        this.modelo = modelo;
        this.matricula = matricula;
    }

    public int getCodCoche() {
        return codCoche;
    }

    public void setCodCoche(int codCoche) {
        this.codCoche = codCoche;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }
}
