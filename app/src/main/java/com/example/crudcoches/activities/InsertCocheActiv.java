package com.example.crudcoches.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.crudcoches.R;
import com.example.crudcoches.controllers.CtrlPpl;

public class InsertCocheActiv extends AppCompatActivity {
    private EditText etMarcaCocheInsert, etModeloCocheInsert, etMatriculaCocheInsert;
    private Button btnInsertar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_coche);

        this.etMarcaCocheInsert = findViewById(R.id.etMarcaCocheInsert);
        this.etModeloCocheInsert = findViewById(R.id.etModeloCocheInsert);
        this.etMatriculaCocheInsert = findViewById(R.id.etMatriculaCocheInsert);
        this.btnInsertar = findViewById(R.id.btnInsertar);


        // EVENTO - Botón insertar
        this.btnInsertar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!etMarcaCocheInsert.getText().toString().isEmpty()
                        && !etModeloCocheInsert.getText().toString().isEmpty()
                        && !etMatriculaCocheInsert.getText().toString().isEmpty()){

                    int result = CtrlPpl.insertarNuevocoche(
                            etMarcaCocheInsert.getText().toString(),
                            etModeloCocheInsert.getText().toString(),
                            etMatriculaCocheInsert.getText().toString());

                    // Nos informa del resultado de la isercción, dependiendo de cuál haya sido.
                    //Sea exitoso o no
                    if(result == 0)
                        Toast.makeText(getApplicationContext(), "No se ha podido insertar el nuevo coche", Toast.LENGTH_LONG).show();
                    else if(result == -1)
                        Toast.makeText(getApplicationContext(), "Este coche ya existe en un nuestra DB", Toast.LENGTH_LONG).show();
                    else
                        Toast.makeText(getApplicationContext(), "Insercción realizada correctamente", Toast.LENGTH_LONG).show();

                }else
                    Toast.makeText(getApplicationContext(), "Debe insertar primero todos los datos del coche", Toast.LENGTH_LONG).show();
            }
        });

    }
}