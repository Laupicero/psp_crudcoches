package com.example.crudcoches.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.crudcoches.R;
import com.example.crudcoches.pojo.Coche;
import com.example.crudcoches.controllers.CtrlPpl;

public class ModifCocheActiv extends AppCompatActivity {
    private Spinner spinMatriculaCocheModif;
    private EditText etModeloCocheModif, etMarcaCocheModif;
    private Button btnModif;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modif_coche);

        // Bindeo
        spinMatriculaCocheModif = findViewById(R.id.spinMatriculaCocheModif);
        etModeloCocheModif = findViewById(R.id.etModeloCocheModif);
        etMarcaCocheModif = findViewById(R.id.etMarcaCocheModif);
        btnModif = findViewById(R.id.btnModif);


        // Relenamos el Spinner
        this.spinMatriculaCocheModif.setAdapter(
                new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, CtrlPpl.getArrayMatriculas())
        );


        //-------------------------
        // EVENTOS
        //-------------------------

        // Seleccionar coche en el spinner
        this.spinMatriculaCocheModif.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                Coche coche = CtrlPpl.getCochebyMatricula(spinMatriculaCocheModif.getSelectedItem().toString());

                if (coche != null) {

                    etModeloCocheModif.setText(coche.getModelo());
                    etMarcaCocheModif.setText(coche.getMarca());

                } else
                    Toast.makeText(getApplicationContext(), "No se ha encontrado nigún coche", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        // BOTÓN PARA MODIFICAR LOS DATOS
        this.btnModif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!etModeloCocheModif.getText().toString().isEmpty() && !etMarcaCocheModif.getText().toString().isEmpty()){
                    int result = CtrlPpl.modificarDatosCoche(etMarcaCocheModif.getText().toString(),
                            etModeloCocheModif.getText().toString(),
                            spinMatriculaCocheModif.getSelectedItem().toString());

                    if(result == 1){
                        Toast.makeText(getApplicationContext(), "Se han modificado los datos del coche", Toast.LENGTH_LONG).show();
                    }

                    else
                        Toast.makeText(getApplicationContext(), "No se han podido modificar los datos", Toast.LENGTH_LONG).show();

                }else
                    Toast.makeText(getApplicationContext(), "Rellene todos los campos", Toast.LENGTH_LONG).show();
            }
        });
    }
}