package com.example.crudcoches.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.example.crudcoches.R;
import com.example.crudcoches.pojo.Coche;
import com.example.crudcoches.adapter.CocheAdapter;
import com.example.crudcoches.controllers.CtrlPpl;

public class ListadoCochesActiv extends AppCompatActivity {
    private RecyclerView rvCochesList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_coches);

        rvCochesList = findViewById(R.id.rvCochesList);

        CocheAdapter adapter = new CocheAdapter(CtrlPpl.getListadoCoches());

        //Le sobreEscribimos el 'listener/click'
        adapter.setListener(new CocheAdapter.OnClickCocheListener() {
            @Override
            public void onItemClicked(Coche item) {
                if (item != null) {
                    Intent actFinPedido = new Intent(ListadoCochesActiv.this, CocheDetallesActiv.class);
                    // Nos creamos el 'bundle' y ponemos la clase como serializable
                    actFinPedido.putExtra("cocheDetalle", item);
                    startActivity(actFinPedido);
                } else {
                    Toast toastmsj = Toast.makeText(getApplicationContext(), "ERROR INESPERADO", Toast.LENGTH_LONG);
                    toastmsj.show();
                }
            }
        });
        rvCochesList.setAdapter(adapter);
    }
}