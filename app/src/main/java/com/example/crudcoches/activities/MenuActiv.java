package com.example.crudcoches.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.crudcoches.R;

public class MenuActiv extends AppCompatActivity {
    private Button btnVerCoches, btnInsertarCoche, btnBuscarCoche, btnBorrarCoches, btnModificCoches;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        this.btnVerCoches = findViewById(R.id.btnVerCoches);
        this.btnInsertarCoche = findViewById(R.id.btnInsertarCoche);
        this.btnBuscarCoche = findViewById(R.id.btnBuscarCoche);
        this.btnModificCoches = findViewById(R.id.btnModificCoches);
        this.btnBorrarCoches = findViewById(R.id.btnBorrarCoches);

        //SharedPreferences
        SharedPreferences prefs =getSharedPreferences("MisPreferencias", Context.MODE_PRIVATE);

        //---------------
        // EVENTOS
        //---------------

        // Vemos nuestro listado de coches
        this.btnVerCoches.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActiv.this, ListadoCochesActiv.class);
                startActivity(intent);
            }
        });

        // Buscamos un coche a través de cu matrícula
        this.btnBuscarCoche.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActiv.this, BuscarCocheActiv.class);
                startActivity(intent);
            }
        });


        // Insertamos un nuevo coche
        // Esta opción sólo estará disponible si nos logeamos cómo empleado
        this.btnInsertarCoche.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nomEmpleado = prefs.getString("nombreEmpleado", "");
                Boolean opEmpleado = prefs.getBoolean("opcionEmpleado", false);

                if(opEmpleado && !nomEmpleado.equalsIgnoreCase("")){
                    Intent intent = new Intent(MenuActiv.this, InsertCocheActiv.class);
                    startActivity(intent);
                }else
                    Toast.makeText(getApplicationContext(), "No puede realizar está función. Debe loguearse cómo empleado para poder realizarla", Toast.LENGTH_LONG).show();
            }
        });


        // Modificamos uno de nuestros coches
        // Esta opción sólo estará disponible si nos logeamos cómo empleado
        this.btnModificCoches.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nomEmpleado = prefs.getString("nombreEmpleado", "");
                Boolean opEmpleado = prefs.getBoolean("opcionEmpleado", false);

                if(opEmpleado && !nomEmpleado.equalsIgnoreCase("")){
                    Intent intent = new Intent(MenuActiv.this, ModifCocheActiv.class);
                    startActivity(intent);
                }else
                    Toast.makeText(getApplicationContext(), "No puede realizar está función. Debe loguearse cómo empleado para poder realizarla", Toast.LENGTH_LONG).show();
            }
        });

        // Borramos un coche
        // Esta opción sólo estará disponible si nos logeamos cómo empleado
        this.btnBorrarCoches.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nomEmpleado = prefs.getString("nombreEmpleado", "");
                Boolean opEmpleado = prefs.getBoolean("opcionEmpleado", false);

                if(opEmpleado && !nomEmpleado.equalsIgnoreCase("")){
                    Intent intent = new Intent(MenuActiv.this, BorrarCocheActiv.class);
                    startActivity(intent);
                }else
                    Toast.makeText(getApplicationContext(), "No puede realizar está función. Debe loguearse cómo empleado para poder realizarla", Toast.LENGTH_LONG).show();
            }
        });
    }
}