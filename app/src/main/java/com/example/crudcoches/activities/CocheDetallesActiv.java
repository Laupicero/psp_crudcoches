package com.example.crudcoches.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.example.crudcoches.R;
import com.example.crudcoches.pojo.Coche;

public class CocheDetallesActiv extends AppCompatActivity {
    private TextView tvDetallesMarcaCoche, tvDetallesTipoCoche, tvDetallesMatriculaCoche;
    private Coche misDatosCoche;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coche_detalles);

        tvDetallesMarcaCoche = findViewById(R.id.tvDetallesMarcaCoche);
        tvDetallesTipoCoche = findViewById(R.id.tvDetallesTipoCoche);
        tvDetallesMatriculaCoche = findViewById(R.id.tvDetallesMatriculaCoche);

        //Obtenemos el Bundle
        this.misDatosCoche = (Coche) getIntent().getSerializableExtra("cocheDetalle");

        // Si no es null, mostramos los datos
        if(misDatosCoche !=null){
            tvDetallesMarcaCoche.setText(misDatosCoche.getMarca());
            tvDetallesTipoCoche.setText(misDatosCoche.getModelo());
            tvDetallesMatriculaCoche.setText(misDatosCoche.getMatricula());
        }
    }
}