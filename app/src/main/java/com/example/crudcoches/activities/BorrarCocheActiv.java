package com.example.crudcoches.activities;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import com.example.crudcoches.R;
import com.example.crudcoches.pojo.Coche;
import com.example.crudcoches.controllers.CtrlPpl;

public class BorrarCocheActiv extends AppCompatActivity {
    private Spinner spinMatriculaCocheBorrar;
    private EditText etMarcaCocheBorrar, etModeloCocheBorrar;
    private Button btnBorrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_borrar_coche);

        this.spinMatriculaCocheBorrar = findViewById(R.id.spinMatriculaCocheBorrar);
        this.btnBorrar = findViewById(R.id.btnBorrar);
        this.etMarcaCocheBorrar = findViewById(R.id.etMarcaCocheBorrar);
        this.etModeloCocheBorrar = findViewById(R.id.etModeloCocheBorrar);

        // Relenamos el Spinner
        rellenarSpinner();

        //---------------------
        // EVENTOS
        //---------------------
        // Seleccionar coche en el spinner
        this.spinMatriculaCocheBorrar.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                Coche coche = CtrlPpl.getCochebyMatricula(spinMatriculaCocheBorrar.getSelectedItem().toString());

                if (coche != null) {

                    etMarcaCocheBorrar.setText(coche.getMarca());
                    etModeloCocheBorrar.setText(coche.getModelo());

                } else
                    Toast.makeText(getApplicationContext(), "No se ha encontrado nigún coche", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {   }
        });


        // BOTÓN PARA ELIMINAR LOS DATOS
        this.btnBorrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!etMarcaCocheBorrar.getText().toString().isEmpty() && !etModeloCocheBorrar.getText().toString().isEmpty()){
                    int result = CtrlPpl.eliminarDatosCoche(spinMatriculaCocheBorrar.getSelectedItem().toString());

                    if(result == 1){
                        Toast.makeText(getApplicationContext(), "Se han eliminado los datos del coche", Toast.LENGTH_LONG).show();
                        rellenarSpinner();
                    }
                    else
                        Toast.makeText(getApplicationContext(), "No se han podido eliminar los datos", Toast.LENGTH_LONG).show();

                }else
                    Toast.makeText(getApplicationContext(), "Rellene todos los campos", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void rellenarSpinner() {
        this.spinMatriculaCocheBorrar.setAdapter(
                new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, CtrlPpl.getArrayMatriculas())
        );
    }
}