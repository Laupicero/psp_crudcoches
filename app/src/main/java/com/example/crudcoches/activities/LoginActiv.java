package com.example.crudcoches.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.example.crudcoches.R;
import com.example.crudcoches.controllers.CtrlPpl;

public class LoginActiv extends AppCompatActivity {
    private EditText userNameLogin, userPassLogin;
    private CheckBox userEmpleadoCB;
    private Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Cargar DB Concesionario
        CtrlPpl.inicializarDB_DAO(getApplicationContext());

        //Bindeo
        this.userNameLogin = findViewById(R.id.userNameLogin);
        this.userPassLogin = findViewById(R.id.userPassLogin);
        this.userEmpleadoCB = findViewById(R.id.userSaveData);
        this.btnLogin = findViewById(R.id.btnLogin);

        //---------------
        // EVENTOS
        //---------------
        this.btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Comprobamos si se ha pulsado el 'checkbox' para así poder acceder como empleado
                // Esta opción te permitirá hacer las opciones del CRUD
                if(userEmpleadoCB.isChecked()){
                    if(!userNameLogin.getText().toString().isEmpty() && !userPassLogin.getText().toString().isEmpty()){
                        SharedPreferences prefs = getSharedPreferences("MisPreferencias", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editorPrefs = prefs.edit();
                        editorPrefs.putString("nombreEmpleado", userNameLogin.getText().toString());
                        editorPrefs.putString("passEmpleado", userPassLogin.getText().toString());
                        editorPrefs.putBoolean("opcionEmpleado", true);
                        editorPrefs.apply();

                        Intent intent = new Intent(LoginActiv.this, MenuActiv.class);
                        startActivity(intent);

                    }else
                        Toast.makeText(getApplicationContext(), "Debe insertar primero sus datos para acceder como empleado", Toast.LENGTH_LONG).show();
                }else{
                    Intent intent = new Intent(LoginActiv.this, MenuActiv.class);
                    startActivity(intent);
                }
            }
        });
    }
}