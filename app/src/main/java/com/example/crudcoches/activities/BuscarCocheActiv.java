package com.example.crudcoches.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.crudcoches.R;
import com.example.crudcoches.pojo.Coche;
import com.example.crudcoches.controllers.CtrlPpl;

public class BuscarCocheActiv extends AppCompatActivity {
    private Spinner spinMatriculaCocheBuscar;
    private LinearLayout layoutContainer;
    private TextView tvMarcaCoche, tvTipoCoche, tvMatriculaCoche;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar_coche);

        this.spinMatriculaCocheBuscar = findViewById(R.id.spinMatriculaCocheBuscar);
        this.layoutContainer = findViewById(R.id.resultDatosBusquedaCoche);
        this.tvMarcaCoche = findViewById(R.id.tvMarcaCoche);
        this.tvTipoCoche = findViewById(R.id.tvTipoCoche);
        this.tvMatriculaCoche = findViewById(R.id.tvMatriculaCoche);

        // Ocultamos el contenedor con los resultados
        this.layoutContainer.setVisibility(View.INVISIBLE);

        // Rellenamos el Spinner
        this.spinMatriculaCocheBuscar.setAdapter(
                new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, CtrlPpl.getArrayMatriculas())
        );


        //-------------------------
        // EVENTOS
        //-------------------------

        this.spinMatriculaCocheBuscar.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                Coche coche = CtrlPpl.getCochebyMatricula(spinMatriculaCocheBuscar.getSelectedItem().toString());

                if(coche != null){
                    layoutContainer.setVisibility(View.VISIBLE);

                    String strMarcaCoche = "MARCA: " + coche.getMarca();
                    String strModeloCoche = "MODELO: " + coche.getModelo();
                    String strMatriculaCoche = "MATRICULA: " + coche.getMatricula();

                    tvMarcaCoche.setText(strMarcaCoche);
                    tvTipoCoche.setText(strModeloCoche);
                    tvMatriculaCoche.setText(strMatriculaCoche);

                }else
                    Toast.makeText(getApplicationContext(), "No se ha encontrado nigún coche", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }
}